import React from "react";

const Item = ({ item }) => {
  return (
    <div className="card m-2">
      <img src={`${process.env.PUBLIC_URL}/assets/images/${item.image}.jpg`} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{item.name}</h5>
        <p className="card-text">
          {item.description}
        </p>
        <a href="#" className="btn btn-primary">
          Add to cart
        </a>
      </div>
    </div>
  );
};

export default Item;
