import React, { Component } from "react";
import Item from "./common/item";
import { getItems } from "../services/items";
import NavBar from "./common/navbar";

class Homepage extends React.Component {
  state = {
    items: getItems(),
  };

  render() {
    return (
      <>
        <NavBar />
        <div className="background">
          <div className="products">
          <div className="item-list">
            {this.state.items.map((e) => {
              return <Item
              item={e}>
              </Item>;
            })}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Homepage;
