import "./App.css";
import Homepage from "./components/homepage";
import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";
import UserProfile from "./components/userprofile";
import LoginPage from "./components/loginpage";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <div className="sans-serif">
          <Route exact  path="/" component={Homepage} />
          <Route path="/edituser/1" component={UserProfile} />
          <Route path="/login" component={LoginPage} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
